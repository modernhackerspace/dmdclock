 //##########################################################################
//## RESET PREP FOR THE SHITTY FM6126A CHIP
//##########################################################################

#define GPIO_PIN_CLK_TEENSY_PIN    14
#define GPIO_PIN_LATCH_TEENSY_PIN   3
#define GPIO_PIN_OE_TEENSY_PIN      4
#define GPIO_PIN_B0_TEENSY_PIN      6
#define GPIO_PIN_R0_TEENSY_PIN      2
#define GPIO_PIN_R1_TEENSY_PIN      21
#define GPIO_PIN_G0_TEENSY_PIN      5
#define GPIO_PIN_G1_TEENSY_PIN      8
#define GPIO_PIN_B1_TEENSY_PIN      20
#define ADDX_TEENSY_PIN_0   9
#define ADDX_TEENSY_PIN_1   10
#define ADDX_TEENSY_PIN_2   22
#define ADDX_TEENSY_PIN_3   23
int MaxLed = 128; 
int C12[16] = {0,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1};
int C13[16] = {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0};


void setupReset(){
   pinMode(GPIO_PIN_CLK_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_LATCH_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_OE_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_B0_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_R0_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_R1_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_G0_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_G1_TEENSY_PIN, OUTPUT);
    pinMode(GPIO_PIN_B1_TEENSY_PIN, OUTPUT);
    pinMode(ADDX_TEENSY_PIN_0, OUTPUT);
    pinMode(ADDX_TEENSY_PIN_1, OUTPUT);
    pinMode(ADDX_TEENSY_PIN_2, OUTPUT);
    pinMode(ADDX_TEENSY_PIN_3, OUTPUT);
    digitalWrite(ADDX_TEENSY_PIN_0, HIGH);
}

void displayReset(){
 digitalWrite (GPIO_PIN_OE_TEENSY_PIN, HIGH); // Display reset
    digitalWrite (GPIO_PIN_LATCH_TEENSY_PIN, LOW);
    digitalWrite (GPIO_PIN_CLK_TEENSY_PIN, LOW);
    for (int l=0; l< MaxLed; l++) {    
      int y=l%16;
      digitalWrite (GPIO_PIN_R0_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_G0_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_B0_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_R1_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_G1_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_B1_TEENSY_PIN,LOW);
     
         if (C12[y]==1) {
          digitalWrite (GPIO_PIN_R0_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_G0_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_B0_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_R1_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_G1_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_B1_TEENSY_PIN,HIGH);
         
      }
    
      
      
      if (l>MaxLed-12){digitalWrite(GPIO_PIN_LATCH_TEENSY_PIN, HIGH);}
          else{digitalWrite(GPIO_PIN_LATCH_TEENSY_PIN, LOW);}
      digitalWrite(GPIO_PIN_CLK_TEENSY_PIN, HIGH);
      digitalWrite(GPIO_PIN_CLK_TEENSY_PIN, LOW);
          
      
    }
    digitalWrite (GPIO_PIN_LATCH_TEENSY_PIN, LOW);
    digitalWrite (GPIO_PIN_CLK_TEENSY_PIN, LOW);
    // Send Data to control register 12
    for (int l=0; l< MaxLed; l++) {    
      int y=l%16;
      digitalWrite (GPIO_PIN_R0_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_G0_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_B0_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_R1_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_G1_TEENSY_PIN,LOW);
      digitalWrite (GPIO_PIN_B1_TEENSY_PIN,LOW);
      if (C13[y]==1){
          digitalWrite (GPIO_PIN_R0_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_G0_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_B0_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_R1_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_G1_TEENSY_PIN,HIGH);
          digitalWrite (GPIO_PIN_B1_TEENSY_PIN,HIGH);
      }  
      if (l>MaxLed-13){digitalWrite(GPIO_PIN_LATCH_TEENSY_PIN, HIGH);}
          else{digitalWrite(GPIO_PIN_LATCH_TEENSY_PIN, LOW);}
      digitalWrite(GPIO_PIN_CLK_TEENSY_PIN, HIGH);
      digitalWrite(GPIO_PIN_CLK_TEENSY_PIN, LOW);
    }
    digitalWrite (GPIO_PIN_LATCH_TEENSY_PIN, LOW);
    digitalWrite (GPIO_PIN_CLK_TEENSY_PIN, LOW);
    Serial.println("ran");
    
    
	}
