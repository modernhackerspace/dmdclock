package sample;

import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Collection;

public class playAnimation {


    public void init(ImageView dmdDisplay, Image[] images, TextField delayField, Timeline timeline, TextField frameCount, int numPics) {


        Collection<KeyFrame> frames = timeline.getKeyFrames();
        Duration delay = Duration.millis(Integer.parseInt(delayField.getText()));
        Duration frameTime = Duration.ZERO;
        frames.clear();

        for (Image img : images) {

            frameTime = frameTime.add(delay);
            frames.add(new KeyFrame(frameTime, event -> dmdDisplay.setImage(img)));
            System.out.println(img);

        }



    }
}