package sample;


import javafx.animation.Animation;

import javafx.animation.Timeline;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;

import javafx.event.EventHandler;

import javafx.fxml.FXML;

import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.image.Image;

import javafx.scene.layout.HBox;

import javafx.stage.FileChooser;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import java.nio.ByteBuffer;

import java.util.List;

public class Controller {

    //image View
    @FXML
    public ImageView dmdDisplay;
    @FXML
    public ImageView dmdDisplay2;
    @FXML
    public ImageView dmdDisplay3;

    //labels
    @FXML
    public Label framecountLabel;
    @FXML
    public Label framecountLabel2;
    @FXML
    public Label framecountLabel3;
    @FXML
    public Label fontNumber;
    //TextField
    @FXML
    public TextField delayField;
    @FXML
    public TextField frameCount;
    @FXML
    public TextField frameCount2;


    @FXML
    public TextField topLeftX;
    @FXML
    public TextField topLeftY;


    @FXML
    public TextField captureWidth;
    @FXML
    public TextField captureHeight;
    @FXML
    public TextField mouseX;
    @FXML
    public TextField mouseY;

    //buttons
    @FXML
    public Button playButton;
    @FXML
    public Button delFrameButton;
    @FXML
    public Button nextFrameButton;
    @FXML
    public Button prevFrameButton;
    @FXML
    public Button gotoFrameButton;
    @FXML
    public Button lastFrameButton;
    @FXML
    public Button firstFrameButton;
    @FXML
    public Button stopButton;
    @FXML
    public Button screenCap;
    @FXML
    public Button loadPictures;
    @FXML
    public Button loadPictures2;
    @FXML Button screenCap2;


    //HBox
    @FXML
    public HBox mouseTest;

    //Menu Items
    @FXML
    private MenuItem loadRaw;


    //images
    public Image[] imageArray = new Image[8000];
    public Image[] imageArray2 = new Image[8000];
    public Image[] fontArray = new Image[8000];

    //Strings
    public String[] vpinmameImageData = new String[8000];
    public String[] vpinmameImageData2 = new String[8000];
    //Integers
    public int frameNumber = 0;
    public int frameNumber2 = 0;
    public int frameNumber3 = 0;
    public int totalPics = 0;
    public int totalPics2 = 0;
    public int totalPics3 = 0;
    public int fontTotal=0;
    public int displayFontFrame=0;


    //Boolean Variables
    public boolean startStop = true;
    public boolean listening;

    //other globals
    ErrorAlert error = new ErrorAlert();
    Timeline timeline = new Timeline();
    public Rectangle captureArea;
    public Rectangle fontArea;
    playAnimation playAnimation = new playAnimation();


    public void initialize() {

    }


    @FXML
    public void firstFrame() {
        frameNumber = 0;
        frameCount.setText(Integer.toString(frameNumber));
        dmdDisplay.setImage(imageArray[frameNumber]);

    }

    @FXML
    public void firstFrame2() {
        frameNumber2 = 0;
        frameCount2.setText(Integer.toString(frameNumber2));
        dmdDisplay2.setImage(imageArray2[frameNumber2]);

    }


    @FXML
    public void prevFrame() {
        try {
            frameNumber = Integer.parseInt(frameCount.getText());
        } catch (RuntimeException e) {
            error.nonInt();
        }
        if (frameNumber == 0) {

        } else if (frameNumber >= totalPics || frameNumber < 0) {
            frameCount.setText("0");
        } else {
            frameNumber -= 1;
            frameCount.setText(Integer.toString(frameNumber));
            dmdDisplay.setImage(imageArray[frameNumber]);
        }
    }

    @FXML
    public void prevFrame2() {
        try {
            frameNumber2 = Integer.parseInt(frameCount2.getText());
        } catch (RuntimeException e) {
            error.nonInt();
        }
        if (frameNumber2 == 0) {

        } else if (frameNumber2 >= totalPics2 || frameNumber2 < 0) {
            frameCount2.setText("0");
        } else {
            frameNumber2 -= 1;
            frameCount2.setText(Integer.toString(frameNumber2));
            dmdDisplay2.setImage(imageArray2[frameNumber2]);
        }
    }


    @FXML
    public void prevFont() {

        if (displayFontFrame > 0) {
            dmdDisplay3.setImage(fontArray[displayFontFrame-1]);
            displayFontFrame-=1;

        } else{
            dmdDisplay3.setImage(fontArray[0]);
        }
        fontNumber.setText(Integer.toString(displayFontFrame));
    }


    @FXML
    public void nextFrame() {
        try {
            frameNumber = Integer.parseInt(frameCount.getText());
        } catch (RuntimeException e) {
            error.nonInt();
        }
        if (frameNumber == totalPics - 1) {

        } else if (frameNumber >= totalPics || frameNumber < 0) {
            frameCount.setText("0");
        } else {
            frameNumber += 1;
            frameCount.setText(Integer.toString(frameNumber));
            dmdDisplay.setImage(imageArray[frameNumber]);

        }
    }

    @FXML
    public void nextFrame2() {
        try {
            frameNumber2 = Integer.parseInt(frameCount2.getText());
        } catch (RuntimeException e) {
            error.nonInt();
        }
        if (frameNumber2 == totalPics2 - 1) {

        } else if (frameNumber2 >= totalPics2 || frameNumber2 < 0) {
            frameCount2.setText("0");
        } else {
            frameNumber2 += 1;
            frameCount2.setText(Integer.toString(frameNumber2));
            dmdDisplay2.setImage(imageArray2[frameNumber2]);

        }
    }

    @FXML
    public void nextFont() {
        int lastFrameTest=0;
        for(int i=0; i<fontArray.length-1;i++) {
            if(fontArray[i]==null){
                lastFrameTest=i;
                System.out.println("lastFrameTest=");
                System.out.println(lastFrameTest);
                break;
            }
        }
        if(displayFontFrame>=lastFrameTest-1) {
            dmdDisplay3.setImage(fontArray[lastFrameTest-1]);
        }else{
            displayFontFrame+=1;
            dmdDisplay3.setImage(fontArray[displayFontFrame]);
        }
        fontNumber.setText(Integer.toString(displayFontFrame));

    }


    @FXML
    public void playFrames() throws Exception {
        timeline.stop();
        playAnimation.init(dmdDisplay, imageArray, delayField, timeline, frameCount, totalPics);
        timeline.play();
        playButton.setDisable(true);
        delFrameButton.setDisable(true);
        nextFrameButton.setDisable(true);
        prevFrameButton.setDisable(true);
        gotoFrameButton.setDisable(true);
        lastFrameButton.setDisable(true);
        firstFrameButton.setDisable(true);


        timeline.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                firstFrame();
                playButton.setDisable(false);
                delFrameButton.setDisable(false);
                nextFrameButton.setDisable(false);
                prevFrameButton.setDisable(false);
                gotoFrameButton.setDisable(false);
                lastFrameButton.setDisable(false);
                firstFrameButton.setDisable(false);
            }
        });
    }


    @FXML
    public void gotoFrame() throws Exception {
        try {
            frameNumber = Integer.parseInt(frameCount.getText());
        } catch (RuntimeException e) {
            error.nonInt();
        }
        try {
            if (Integer.parseInt(frameCount.getText()) > totalPics - 1 || Integer.parseInt(frameCount.getText()) < 0) {
                Alert outofBounds = new Alert(Alert.AlertType.ERROR);
                outofBounds.setTitle("Error");
                outofBounds.setContentText("Has to be bigger than 0 and smaller than " + Integer.toString(totalPics - 1));
                outofBounds.show();
            } else {
                dmdDisplay.setImage(imageArray[frameNumber]);
            }

        } catch (RuntimeException e) {
            Alert outofBounds = new Alert(Alert.AlertType.ERROR);
            outofBounds.setTitle("Error");
            outofBounds.setContentText("Has to be bigger than 0 and smaller than " + Integer.toString(totalPics - 1));
            outofBounds.show();
        }
    }

    @FXML
    public void gotoFrame2() throws Exception {
        try {
            frameNumber2 = Integer.parseInt(frameCount2.getText());
        } catch (RuntimeException e) {
            error.nonInt();
        }
        try {
            if (Integer.parseInt(frameCount2.getText()) > totalPics2 - 1 || Integer.parseInt(frameCount2.getText()) < 0) {
                Alert outofBounds = new Alert(Alert.AlertType.ERROR);
                outofBounds.setTitle("Error");
                outofBounds.setContentText("Has to be bigger than 0 and smaller than " + Integer.toString(totalPics2 - 1));
                outofBounds.show();
            } else {
                dmdDisplay2.setImage(imageArray2[frameNumber2]);
            }
        } catch (RuntimeException e) {
            Alert outofBounds = new Alert(Alert.AlertType.ERROR);
            outofBounds.setTitle("Error");
            outofBounds.setContentText("Has to be bigger than 0 and smaller than " + Integer.toString(totalPics2 - 1));
            outofBounds.show();
        }
    }




    @FXML
    public void lastFrame() {
        frameNumber = totalPics - 1;
        frameCount.setText(Integer.toString(frameNumber));
        dmdDisplay.setImage(imageArray[frameNumber]);

    }

    @FXML
    public void lastFrame2() {
        frameNumber2 = totalPics2 - 1;
        frameCount2.setText(Integer.toString(frameNumber2));
        dmdDisplay2.setImage(imageArray2[frameNumber2]);

    }


    @FXML
    public void deleteFrame() throws Exception {
        gotoFrame();
        for (int i = frameNumber; i < imageArray.length - 1; i++) {
            imageArray[i] = imageArray[i + 1];

        }
        int frames = totalPics - 1;
        if (totalPics > 0) {
            if (frames>0) {
                frames -= 1;
            }else{
                frames = 0;
            }
            framecountLabel.setText("out of " + Integer.toString(frames) + " frames");
        }
        totalPics = frames + 1;
        dmdDisplay.setImage(imageArray[frameNumber]);
    }

    @FXML
    public void deleteRemaining() throws Exception {
        gotoFrame();
        for (int i = frameNumber; i < imageArray.length - 1; i++) {
            imageArray[i+1] = null;

        }
        if(totalPics==0) {
            totalPics = frameNumber-1;
        }else{
            totalPics = frameNumber;
        }
        framecountLabel.setText("out of " + Integer.toString(totalPics) + " frames");
        dmdDisplay.setImage(imageArray[frameNumber]);
    }

    @FXML
    public void deleteBefore() throws Exception {
        gotoFrame();
        int x=0;
        for (int i = frameNumber; i <= imageArray.length - 1; i++) {
            imageArray[x] = imageArray[i];
            x+=1;
        }
        if (totalPics >0){
            totalPics = totalPics-frameNumber-1;
        } else{
            totalPics=totalPics-frameNumber;
        }

        framecountLabel.setText("out of " + Integer.toString(totalPics) + " frames");
        dmdDisplay.setImage(imageArray[frameNumber]);
    }

    @FXML
    public void deleteFrame2() throws Exception {
        gotoFrame2();
        for (int i = frameNumber2; i < imageArray2.length - 1; i++) {
            imageArray2[i] = imageArray2[i + 1];

        }
        int frames = totalPics2 - 1;
        if (totalPics2 > 0) {
            frames -= 1;
            framecountLabel2.setText("out of " + Integer.toString(frames) + " frames");
        }
        totalPics2 = frames + 1;
        dmdDisplay2.setImage(imageArray2[frameNumber2]);
    }

    @FXML
    public void deleteFont() throws Exception {
        for (int i = 0; i < fontArray.length - 1; i++) {
            fontArray[i] = null;

        }
        fontTotal=0;
        displayFontFrame=0;
        dmdDisplay3.setImage(fontArray[displayFontFrame]);
        fontNumber.setText("0");
        framecountLabel3.setText("Out of: 0");
    }


    @FXML
    public void insertFrame() {
        Image imageToInsert = imageArray2[Integer.parseInt(frameCount2.getText())];
        int locationToInsert = Integer.parseInt(frameCount.getText());

        for (int i = imageArray.length - 1; i > locationToInsert; i--) {
            imageArray[i] = imageArray[i - 1];
        }
        imageArray[locationToInsert] = imageToInsert;
        populateImageArray();
        frameCount.setText(Integer.toString(locationToInsert));
        dmdDisplay.setImage(imageArray[locationToInsert]);

    }


    @FXML
    public void stopAnimation() {
        playButton.setDisable(false);
        delFrameButton.setDisable(false);
        nextFrameButton.setDisable(false);
        prevFrameButton.setDisable(false);
        gotoFrameButton.setDisable(false);
        lastFrameButton.setDisable(false);
        firstFrameButton.setDisable(false);
        if (timeline.getStatus() == Animation.Status.RUNNING) {
            timeline.stop();

            try {
                String stopValue = dmdDisplay.getImage().toString().trim();
                for (int i = 0; i < totalPics - 1; i++) {
                    try {
                        String testValue = imageArray[i].toString().trim();

                        if (testValue.equals(stopValue)) {
                            frameCount.setText(Integer.toString(i));

                            break;
                        }

                    } catch (RuntimeException e) {

                    }
                }
            } catch (RuntimeException e) {
                dmdDisplay.setImage(imageArray[0]);
                frameCount.setText("0");
            }

        }
    }


    @FXML
    public void loadFiles() throws FileNotFoundException {

        for (int i = 0; i < imageArray.length - 1; i++) {
            imageArray[i] = null;
        }


        FileChooser files = new FileChooser();


        List<File> pics = files.showOpenMultipleDialog(loadPictures.getScene().getWindow());

        for (int i = 0; i < pics.size(); i++) {
            imageArray[i] = new Image(new FileInputStream(pics.get(i).toString()));
        }
        totalPics = pics.size();

        dmdDisplay.setImage(imageArray[0]);
        frameCount.setText("0");
        framecountLabel.setText("out of " + Integer.toString(pics.size() - 1) + " frames");
    }

    @FXML
    public void loadFiles2() throws FileNotFoundException {

        for (int i = 0; i < imageArray2.length - 1; i++) {
            imageArray2[i] = null;
        }


        FileChooser files = new FileChooser();


        List<File> pics2 = files.showOpenMultipleDialog(loadPictures2.getScene().getWindow());

        for (int i = 0; i < pics2.size(); i++) {
            imageArray2[i] = new Image(new FileInputStream(pics2.get(i).toString()));
        }
        totalPics2 = pics2.size();

        dmdDisplay2.setImage(imageArray2[0]);
        frameCount2.setText("0");
        framecountLabel2.setText("out of " + Integer.toString(pics2.size() - 1) + " frames");
    }


    @FXML
    public void populateImageArray() {

        for (int i = 0; i < imageArray.length; i++) {
            if (imageArray[i] == null) {
                totalPics = i;
                frameCount.setText(Integer.toString(0));
                if(totalPics>0) {
                    framecountLabel.setText("out of " + Integer.toString(totalPics - 1) + " frames");
                } else{
                    framecountLabel.setText("out of " + Integer.toString(totalPics) + " frames");
                }
                break;
            }
        }
    }

    @FXML
    public void populateImageArray2() {

        for (int i = 0; i < imageArray2.length; i++) {
            if (imageArray2[i] == null) {
                totalPics2 = i;
                frameCount2.setText(Integer.toString(0));
                framecountLabel2.setText("out of " + Integer.toString(totalPics2 - 1) + " frames");

                break;
            }
        }
    }




    @FXML
    public void captureFrame() {

        int x = MouseInfo.getPointerInfo().getLocation().x;
        int y = MouseInfo.getPointerInfo().getLocation().y;

        topLeftX.setText(Integer.toString(x));
        topLeftY.setText(Integer.toString(y));
        mouseX.setText(Integer.toString(x));
        mouseY.setText(Integer.toString(y));
    }




    public void fontCap(ActionEvent mouseEvent) {

        int xWidth = Integer.parseInt(captureWidth.getText());
        int yHeight = Integer.parseInt(captureHeight.getText());

        int x = Integer.parseInt(mouseX.getText());
        int y = Integer.parseInt(mouseY.getText());
        fontArea = new Rectangle(x,y, xWidth,yHeight);

        try {
            Robot test = new Robot();
            BufferedImage rawFont = new BufferedImage(20, 20, BufferedImage.TYPE_INT_RGB);
            BufferedImage scaledFont = new BufferedImage(14, 18, rawFont.getType());

            rawFont = test.createScreenCapture(fontArea);
            Graphics2D g2d = scaledFont.createGraphics();
            g2d.drawImage(rawFont, 0, 0, 14, 18, null);
            g2d.dispose();


            fontArray[fontTotal] = SwingFXUtils.toFXImage(scaledFont, null);
            fontNumber.setText(Integer.toString(fontTotal));
            displayFontFrame= fontTotal;
            framecountLabel3.setText("Out of: " + fontTotal);
            dmdDisplay3.setImage(fontArray[displayFontFrame]);
            fontTotal+=1;
        } catch (Exception e) {
            //   System.out.println("Error in capturing");
        }

    }


    public void captureImage(ActionEvent mouseEvent) {
        int x = Integer.parseInt(topLeftX.getText());
        int y = Integer.parseInt(topLeftY.getText());
        captureArea = new Rectangle(x, y, 512, 128);

        try {
            Robot test = new Robot();
            BufferedImage image = new BufferedImage(128, 32, BufferedImage.TYPE_INT_RGB);
            BufferedImage scaledImage = new BufferedImage(128, 32, image.getType());

            System.out.println(image.getWidth());
            image = test.createScreenCapture(captureArea);
            Graphics2D g2d = scaledImage.createGraphics();
            g2d.drawImage(image, 0, 0, 128, 32, null);
            g2d.dispose();

            imageArray[Integer.parseInt(frameCount.getText())] = SwingFXUtils.toFXImage(scaledImage, null);
            //imageArray[Integer.parseInt(frameCount.getText())] = SwingFXUtils.toFXImage(image, null);
            int addFrame = Integer.parseInt(frameCount.getText());

            addFrame += 1;

            frameCount.setText(Integer.toString(addFrame));
            dmdDisplay.setImage(imageArray[addFrame - 1]);

        } catch (Exception e) {
            //   System.out.println("Error in capturing");
        }

    }

    public void captureImage2(ActionEvent mouseEvent) {
        int x = Integer.parseInt(topLeftX.getText());
        int y = Integer.parseInt(topLeftY.getText());
        captureArea = new Rectangle(x, y, 512, 128);

        try {
            Robot test = new Robot();
            BufferedImage image = new BufferedImage(128, 32, BufferedImage.TYPE_INT_RGB);
            BufferedImage scaledImage = new BufferedImage(128, 32, image.getType());

            System.out.println(image.getWidth());
            image = test.createScreenCapture(captureArea);
            Graphics2D g2d = scaledImage.createGraphics();
            g2d.drawImage(image, 0, 0, 128, 32, null);
            g2d.dispose();

            imageArray2[Integer.parseInt(frameCount2.getText())] = SwingFXUtils.toFXImage(scaledImage, null);
            //imageArray[Integer.parseInt(frameCount.getText())] = SwingFXUtils.toFXImage(image, null);
            int addFrame = Integer.parseInt(frameCount2.getText());

            addFrame += 1;

            frameCount2.setText(Integer.toString(addFrame));
            dmdDisplay2.setImage(imageArray2[addFrame - 1]);

        } catch (Exception e) {
            //   System.out.println("Error in capturing");
        }

    }


    @FXML
    public void loadVpinmame() {
        for (int i = 0; i < vpinmameImageData.length - 1; i++) {
            vpinmameImageData[i] = null;


        }

        for (int i = 0; i < imageArray.length - 1; i++) {
            imageArray[i] = null;
        }

        FileChooser file = new FileChooser();
        File dir = new File("\\");
        file.setInitialDirectory(dir);
        file.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("VpinMame text file", "*"));
        File imageFile = file.showOpenDialog(dmdDisplay.getScene().getWindow());
        try {
            FileReader fr = new FileReader(imageFile);
            BufferedReader textFile = new BufferedReader(fr);
            String line;
            String memGarbage;
            int lineCounter = 0;
            int sceneImage = 0;
            while ((line = textFile.readLine()) != null) {
                if (line.length() < 128) {
                    memGarbage = line;

                } else {

                    if (vpinmameImageData[sceneImage] != null) {
                        vpinmameImageData[sceneImage] += line;
                        lineCounter++;
                    } else {
                        vpinmameImageData[sceneImage] = line;
                        lineCounter++;
                    }
                    if (lineCounter > 31) {
                        sceneImage++;
                        lineCounter = 0;

                    }
                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        int x = 0;
        int y = 0;
        int sceneFrame = 0;
        for (String sceneitem : vpinmameImageData) {

            WritableImage test = new WritableImage(128, 32);
            PixelWriter pw = test.getPixelWriter();
            try {
                for (int i = 0; i <= sceneitem.length() - 1; i++) {


                    if (sceneitem.charAt(i) == '1') {
                        pw.setColor(x, y, javafx.scene.paint.Color.rgb(85, 0, 0));

                    } else if (sceneitem.charAt(i) == '2') {
                        pw.setColor(x, y, javafx.scene.paint.Color.rgb(170, 0, 0));

                    } else if (sceneitem.charAt(i) == '3') {
                        pw.setColor(x, y, javafx.scene.paint.Color.rgb(255, 0, 0));

                    } else {
                        pw.setColor(x, y, javafx.scene.paint.Color.rgb(0, 0, 0));

                    }
                    x++;

                    if (x == 128) {

                        x = 0;
                        y++;
                    }
                    if (y == 32) {
                        y = 0;
                        imageArray[sceneFrame] = test;
                        sceneFrame++;

                    }

                }

            } catch (RuntimeException e) {

            }
        }
        dmdDisplay.setImage(imageArray[0]);

        populateImageArray();


    }

    @FXML
    public void loadVpinmame2() {

        for (int i = 0; i < vpinmameImageData2.length - 1; i++) {
            vpinmameImageData2[i] = null;
        }

        for (int i = 0; i < imageArray2.length - 1; i++) {
            imageArray2[i] = null;
        }


        FileChooser file2 = new FileChooser();
        File dir2 = new File("\\");
        file2.setInitialDirectory(dir2);
        file2.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("VpinMame text file", "*.txt"));
        File imageFile2 = file2.showOpenDialog(dmdDisplay2.getScene().getWindow());
        try {
            FileReader fr2 = new FileReader(imageFile2);
            BufferedReader textFile2 = new BufferedReader(fr2);
            String line2;
            String memGarbage2;
            int lineCounter2 = 0;
            int sceneImage2 = 0;
            while ((line2 = textFile2.readLine()) != null) {
                if (line2.length() < 128) {
                    memGarbage2 = line2;

                } else {

                    if (vpinmameImageData2[sceneImage2] != null) {
                        vpinmameImageData2[sceneImage2] += line2;
                        lineCounter2++;
                    } else {
                        vpinmameImageData2[sceneImage2] = line2;
                        lineCounter2++;
                    }
                    if (lineCounter2 > 31) {
                        sceneImage2++;
                        lineCounter2 = 0;

                    }
                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        int x2 = 0;
        int y2 = 0;
        int sceneFrame2 = 0;
        for (String sceneitem2 : vpinmameImageData2) {

            WritableImage test2 = new WritableImage(128, 32);
            PixelWriter pw2 = test2.getPixelWriter();
            try {
                for (int i = 0; i <= sceneitem2.length() - 1; i++) {


                    if (sceneitem2.charAt(i) == '1') {
                        pw2.setColor(x2, y2, javafx.scene.paint.Color.rgb(85, 0, 0));

                    } else if (sceneitem2.charAt(i) == '2') {
                        pw2.setColor(x2, y2, javafx.scene.paint.Color.rgb(170, 0, 0));

                    } else if (sceneitem2.charAt(i) == '3') {
                        pw2.setColor(x2, y2, javafx.scene.paint.Color.rgb(255, 0, 0));

                    } else {
                        pw2.setColor(x2, y2, javafx.scene.paint.Color.rgb(0, 0, 0));

                    }
                    x2++;

                    if (x2 == 128) {

                        x2 = 0;
                        y2++;
                    }
                    if (y2 == 32) {
                        y2 = 0;
                        imageArray2[sceneFrame2] = test2;
                        sceneFrame2++;

                    }

                }

            } catch (RuntimeException e) {

            }
        }
        dmdDisplay2.setImage(imageArray2[0]);

        populateImageArray2();


    }



    @FXML
    public void saveAnimation() {
        int i = 0;
        String pixelColor;

        FileChooser fchooser = new FileChooser();
        File dir2 = new File("\\");
        fchooser.setInitialDirectory(dir2);

        fchooser.setTitle("Save Animation File");

        File saveFile = fchooser.showSaveDialog(dmdDisplay.getScene().getWindow());
        if (saveFile != null) {

            try {
                PrintWriter outFile = new PrintWriter(saveFile);
                //This adds the delay to the first three bytes of the file.
                outFile.print(delayField.getText());
                for (Image imageToSave : imageArray) {
                    i++;
                    if (imageToSave == null) {


                    } else {
                        PixelReader pr = imageToSave.getPixelReader();
                        System.out.println("image width:");
                        System.out.println(imageToSave.getWidth());
                        for (int y = 0; y < 32; y++) {
                            for (int x = 0; x < 128; x++) {
                                pixelColor = pr.getColor(x, y).toString();

                                String darkRed = pixelColor.substring(2, 4);
                                int redValue = Integer.parseInt(darkRed, 16);

                                if (redValue <= Integer.parseInt("10", 16) && redValue > 0) {
                                    outFile.print("0");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("40", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("10", 16)) {
                                    outFile.print("2");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("70", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("40", 16)) {
                                    outFile.print("3");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("7c", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("70", 16)) {
                                    outFile.print("4");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("9b", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("7c", 16)) {
                                    outFile.print("5");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("ba", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("9b", 16)) {
                                    outFile.print("6");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("E5", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("ba", 16)) {
                                    outFile.print("7");

                                } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("ff", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("e5", 16)) {
                                  outFile.print("8");


                                } else {
                                    outFile.print("0");

                                }
                                if (x == 127) {
                                    outFile.println("");
                                }
                            }
                        }
                        // outFile.println("0xffffffff");
                    }
                }
                outFile.close();
            } catch (IOException e) {
                //    System.out.println("no file");
            }
        }
    }

    @FXML
    public void saveFont() {
        int i = 0;
        String pixelColor;

        FileChooser fchooser = new FileChooser();
        File dir2 = new File("\\");
        fchooser.setInitialDirectory(dir2);

        fchooser.setTitle("Save Font File");

        File saveFile = fchooser.showSaveDialog(dmdDisplay3.getScene().getWindow());
        if (saveFile != null) {

            try {
                PrintWriter outFile = new PrintWriter(saveFile);

                for (Image imageToSave : fontArray) {
                    i++;
                    if (imageToSave == null) {


                    } else {
                        PixelReader pr = imageToSave.getPixelReader();

                        try {
                            int xwidth = 14;
                            int yheight = 18;
                            System.out.println(xwidth);
                            System.out.println(yheight);
                            outFile.println("\n");
                            outFile.print("\"");
                            for (int y = 0; y <yheight; y++) {

                                for (int x = 0; x < xwidth; x++) {
                                    pixelColor = pr.getColor(x, y).toString();
                                    System.out.println("where");
                                    System.out.println(x);
                                    String darkRed = pixelColor.substring(2, 4);
                                    int redValue = Integer.parseInt(darkRed, 16);

                                 

                                    if (redValue <= Integer.parseInt("10", 16) && redValue > 0) {
                                        outFile.print("0");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("40", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("10", 16)) {
                                        outFile.print("2");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("70", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("40", 16)) {
                                        outFile.print("3");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("7c", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("70", 16)) {
                                        outFile.print("4");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("9b", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("7c", 16)) {
                                        outFile.print("5");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("ba", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("9b", 16)) {
                                        outFile.print("6");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("E5", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("ba", 16)) {
                                        outFile.print("7");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("ff", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("e5", 16)) {
                                        outFile.print("8");



                                    } else {
                                        outFile.print("0");

                                    }
                                    if (x == xwidth) {
                                        //   outFile.println("");
                                    }

                                }
                                if (y == yheight) {

                                    outFile.println("");


                                }
                            }outFile.print("\"");
                            outFile.print(",");
                        } catch (RuntimeException e) {
                            System.out.println("non int");
                        }
                    }
                }
                outFile.close();
            } catch (IOException e) {
                //    System.out.println("no file");
            }
        }
    }


    @FXML
    public void loadDotClk() {

        for (int i = 0; i < vpinmameImageData.length - 1; i++) {
            vpinmameImageData[i] = null;
        }

        for (int i = 0; i < imageArray.length - 1; i++) {
            imageArray[i] = null;
        }

        File dotClkDir = new File("\\");
        FileChooser fchooser = new FileChooser();
        fchooser.setInitialDirectory(dotClkDir);
        fchooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("DotClock file", "*.scn"));
        File scnFile = fchooser.showOpenDialog(dmdDisplay.getScene().getWindow());

        byte[] version = new byte[4];
        byte[] countItemDotmap = new byte[4];
        int intCountItemDotmap;
        byte[] countItemStory = new byte[4];
        int intCountItemStory;

        byte[] firstFrameDelay = new byte[4];
        byte[] firstFrameLayer = new byte[4];
        byte[] frameDelay = new byte[4];
        byte[] frameLayer = new byte[4];
        byte[] lastFrameDelay = new byte[4];
        byte[] lastFrameLayer = new byte[4];
        byte[] lastBlank = new byte[4];
        byte[] clockStyle = new byte[4];
        byte[] customX = new byte[4];
        byte[] customY = new byte[4];
        byte[] garbage = new byte[20];


        try {
            FileInputStream fileStream = new FileInputStream(scnFile);
            byte[] scnFileByteArray = new byte[(int) (scnFile.length() - 1)];
            fileStream.read(scnFileByteArray, 0, (int) scnFile.length() - 1);
            ByteArrayInputStream scnFileByteStream = new ByteArrayInputStream(scnFileByteArray);
            scnFileByteStream.read(version, 0, 2);
            scnFileByteStream.read(countItemDotmap, 0, 2);
            scnFileByteStream.read(countItemStory, 0, 2);


            intCountItemDotmap = ByteBuffer.wrap(countItemDotmap, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            intCountItemStory = ByteBuffer.wrap(countItemStory, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();

            for (int dotScene = 0; dotScene < intCountItemStory; dotScene++) {
                scnFileByteStream.read(firstFrameDelay, 0, 2);
                scnFileByteStream.read(firstFrameLayer, 0, 2);
                scnFileByteStream.read(frameDelay, 0, 2);
                scnFileByteStream.read(frameLayer, 0, 2);
                scnFileByteStream.read(lastFrameDelay, 0, 2);
                scnFileByteStream.read(lastFrameLayer, 0, 2);
                scnFileByteStream.read(lastBlank, 0, 2);
                scnFileByteStream.read(clockStyle, 0, 1);
                scnFileByteStream.read(customX, 0, 1);
                scnFileByteStream.read(customY, 0, 1);
                scnFileByteStream.read(garbage, 0, 19);
            }

            byte[] dmpWidth = new byte[4];
            byte[] dmpHeight = new byte[4];
            byte[] dmpBpp = new byte[4];
            byte[] hasMask = new byte[4];

            for (int i = 0; i < intCountItemDotmap; i++) {
                scnFileByteStream.read(dmpWidth, 0, 2);
                scnFileByteStream.read(dmpHeight, 0, 2);
                scnFileByteStream.read(dmpBpp, 0, 2);
                scnFileByteStream.read(hasMask, 0, 2);
                int hasMaskInt = ByteBuffer.wrap(hasMask, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();

                //Sanity check
                //  System.out.println(ByteBuffer.wrap(dmpWidth,0,4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt());


                WritableImage dotClkImage = new WritableImage(128, 32);
                PixelWriter pw = dotClkImage.getPixelWriter();
                for (int y = 0; y < 32; y++) {
                    for (int x = 0; x < 128; x += 2) {
                        byte[] value = new byte[4];
                        scnFileByteStream.read(value, 0, 1);

                        int hexInt = ByteBuffer.wrap(value, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();

                        pw.setColor(x, y, javafx.scene.paint.Color.rgb(hexInt, 0, 0));
                        pw.setColor(x + 1, y, javafx.scene.paint.Color.rgb(hexInt, 0, 0));
                    }
                }
                if (hasMaskInt == 1) {
                    for (int y = 0; y < 32; y++) {
                        for (int x = 0; x < 128; x += 8) {
                            byte[] junk = new byte[1];
                            scnFileByteStream.read(junk, 0, 1);

                        }
                    }
                }

                imageArray[i] = dotClkImage;
            }
            dmdDisplay.setImage(imageArray[0]);
            populateImageArray();

        } catch (IOException e) {
            // System.out.println("File read error");
        }

    }



    public void loadForSave(File loadedFile) {

        for (int i = 0; i < vpinmameImageData.length - 1; i++) {
            vpinmameImageData[i] = null;
        }

        for (int i = 0; i < imageArray.length - 1; i++) {
            imageArray[i] = null;
        }

        File scnFile = loadedFile;

        byte[] version = new byte[4];
        byte[] countItemDotmap = new byte[4];
        int intCountItemDotmap;
        byte[] countItemStory = new byte[4];
        int intCountItemStory;

        byte[] firstFrameDelay = new byte[4];
        byte[] firstFrameLayer = new byte[4];
        byte[] frameDelay = new byte[4];
        byte[] frameLayer = new byte[4];
        byte[] lastFrameDelay = new byte[4];
        byte[] lastFrameLayer = new byte[4];
        byte[] lastBlank = new byte[4];
        byte[] clockStyle = new byte[4];
        byte[] customX = new byte[4];
        byte[] customY = new byte[4];
        byte[] garbage = new byte[20];


        try {
            FileInputStream fileStream = new FileInputStream(scnFile);
            byte[] scnFileByteArray = new byte[(int) (scnFile.length() - 1)];
            fileStream.read(scnFileByteArray, 0, (int) scnFile.length() - 1);
            ByteArrayInputStream scnFileByteStream = new ByteArrayInputStream(scnFileByteArray);
            scnFileByteStream.read(version, 0, 2);
            scnFileByteStream.read(countItemDotmap, 0, 2);
            scnFileByteStream.read(countItemStory, 0, 2);


            intCountItemDotmap = ByteBuffer.wrap(countItemDotmap, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            intCountItemStory = ByteBuffer.wrap(countItemStory, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();

            for (int dotScene = 0; dotScene < intCountItemStory; dotScene++) {
                scnFileByteStream.read(firstFrameDelay, 0, 2);
                scnFileByteStream.read(firstFrameLayer, 0, 2);
                scnFileByteStream.read(frameDelay, 0, 2);
                scnFileByteStream.read(frameLayer, 0, 2);
                scnFileByteStream.read(lastFrameDelay, 0, 2);
                scnFileByteStream.read(lastFrameLayer, 0, 2);
                scnFileByteStream.read(lastBlank, 0, 2);
                scnFileByteStream.read(clockStyle, 0, 1);
                scnFileByteStream.read(customX, 0, 1);
                scnFileByteStream.read(customY, 0, 1);
                scnFileByteStream.read(garbage, 0, 19);
            }

            byte[] dmpWidth = new byte[4];
            byte[] dmpHeight = new byte[4];
            byte[] dmpBpp = new byte[4];
            byte[] hasMask = new byte[4];

            for (int i = 0; i < intCountItemDotmap; i++) {
                scnFileByteStream.read(dmpWidth, 0, 2);
                scnFileByteStream.read(dmpHeight, 0, 2);
                scnFileByteStream.read(dmpBpp, 0, 2);
                scnFileByteStream.read(hasMask, 0, 2);
                int hasMaskInt = ByteBuffer.wrap(hasMask, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();

                //Sanity check
                //  System.out.println(ByteBuffer.wrap(dmpWidth,0,4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt());


                WritableImage dotClkImage = new WritableImage(128, 32);
                PixelWriter pw = dotClkImage.getPixelWriter();
                for (int y = 0; y < 32; y++) {
                    for (int x = 0; x < 128; x += 2) {
                        byte[] value = new byte[4];
                        scnFileByteStream.read(value, 0, 1);

                        int hexInt = ByteBuffer.wrap(value, 0, 4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();

                        pw.setColor(x, y, javafx.scene.paint.Color.rgb(hexInt, 0, 0));
                        pw.setColor(x + 1, y, javafx.scene.paint.Color.rgb(hexInt, 0, 0));
                    }
                }
                if (hasMaskInt == 1) {
                    for (int y = 0; y < 32; y++) {
                        for (int x = 0; x < 128; x += 8) {
                            byte[] junk = new byte[1];
                            scnFileByteStream.read(junk, 0, 1);

                        }
                    }
                }

                imageArray[i] = dotClkImage;
            }


        } catch (IOException e) {
            // System.out.println("File read error");
        }


    }



    @FXML
    public void saveAll() {
        int i = 0;
        String pixelColor;
        System.out.println("Did this go?");
        FileChooser files = new FileChooser();
        File dir2 = new File("\\");
        files.setInitialDirectory(dir2);

        List<File> loadFiles = files.showOpenMultipleDialog(dmdDisplay.getScene().getWindow());


        files.setTitle("Save a bunch of files");

        for (int z = 0; z < loadFiles.size() - 1; z++) {
            System.out.println(loadFiles.size());
            File loadFile = loadFiles.get(z);
            System.out.println("is this hitting?");
            loadForSave(loadFile);

            File saveFile = new File("\\animations\\" + z);
            if (saveFile != null) {

                try {

                    PrintWriter outFile = new PrintWriter(saveFile);

                    for (Image imageToSave : imageArray) {
                        i++;
                        if (imageToSave == null) {


                        } else {
                            PixelReader pr = imageToSave.getPixelReader();
                            for (int y = 0; y < 32; y++) {
                                for (int x = 0; x < 128; x++) {
                                    pixelColor = pr.getColor(x, y).toString();

                                    String darkRed = pixelColor.substring(2, 4);
                                    int redValue = Integer.parseInt(darkRed, 16);

                                    if (redValue <= Integer.parseInt("10", 16) && redValue > 0) {
                                        outFile.print("0");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("40", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("10", 16)) {
                                        outFile.print("2");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("70", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("40", 16)) {
                                        outFile.print("3");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("7c", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("70", 16)) {
                                        outFile.print("4");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("9b", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("7c", 16)) {
                                        outFile.print("5");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("ba", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("9b", 16)) {
                                        outFile.print("6");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("E5", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("ba", 16)) {
                                        outFile.print("7");

                                    } else if (Integer.parseInt(darkRed, 16) <= Integer.parseInt("ff", 16) && Integer.parseInt(darkRed, 16) > Integer.parseInt("e5", 16)) {
                                        outFile.print("8");
                                    }else {
                                        outFile.print("0");

                                    }
                                    if (x == 127) {
                                        outFile.println("");
                                    }
                                }
                            }
                            // outFile.println("0xffffffff");
                        }
                    }
                    outFile.close();

                } catch (IOException e) {
                    //    System.out.println("no file");
                }
            }
        }
    }
}







