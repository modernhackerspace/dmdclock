<h1>DMD clock</h1>

This is the repo for the DMD clock.

<b> Check out the vid here: </b>
https://youtu.be/73xbHBJpv74

If you like please subscribe or check out my Patreon site at: 
https://www.patreon.com/modernhackerspace

<h2>Content Details: </h2>
Source code for the animation extractor is located in the folder: "src"

Source code for the arduino code is in the "workingFinalClock" folder

A working version of the animation extractor is in the folder "pinballCreator_jar-mac" or "pinballCreator_jar-windows" 
depending on the os you are working from.

to run: ensure you must have the  **Java 8 sdk/runtime**  environment and from the terminal/commandline type:

```
java -jar /path/to/file/pinballAnimationExtractor.jar
```

or: 

load the project in intellij with the latest java sdk

To load the clock software install the arduino IDE and open the workingFinalClock.ino file

You must have the proper folder installed to export animations for DotClk (c:\animations or on MAC //User//animations).  
If you are on MAC you must run the program as sudo from terminal to save the animations.

<h2> Detailed instructions </h2>
Please find detailed instructions in the pdf: 

```
DMD matrix build and Instructions.pdf
```
