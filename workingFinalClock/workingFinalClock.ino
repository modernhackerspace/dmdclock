#include <SmartLEDShieldV4.h>  // comment out this line for if you're not using SmartLED Shield V4 hardware (this line needs to be before #include <SmartMatrix3.h>)
#include <SmartMatrix3.h>
#include <String.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <Bounce.h>
#include <SD.h>
#include <SPI.h>
#include "reset.h"
#include "games.h"
#include "fonts.h"
//include for GIF
#include "GifDecoder.h"
#include "FilenameFunctions.h"
#define DISPLAY_TIME_SECONDS 10
#include "MemoryFree.h"
#define ENABLE_SCROLLING  1

//this sets up the built in SD card on the teensy3.6
#define SD_CARD BUILTIN_SDCARD

//default settings from the SmartLED shield library
#define COLOR_DEPTH 24                  // known working: 24, 48 - If the sketch uses type `rgb24` directly, COLOR_DEPTH must be 24

//this is the file created for loading the data from the SD card
File myFile;

//Set up for the LED panels
const uint8_t kMatrixWidth = 128;        // 128 leds wide
const uint8_t kMatrixHeight = 32;       // 32 LEDs high
const uint8_t kRefreshDepth = 36;       // refresh depth default working@ 36
const uint8_t kDmaBufferRows = 4;       // known working: 2-4, use 2 to save memory, more to keep from dropping frames and automatically lowering refresh rate
const uint8_t kPanelType = SMARTMATRIX_HUB75_32ROW_MOD16SCAN; // use SMARTMATRIX_HUB75_16ROW_MOD8SCAN for common 16x32 panels, or use SMARTMATRIX_HUB75_64ROW_MOD32SCAN for common 64x64 panels
//these are default settings
const uint8_t kMatrixOptions = (SMARTMATRIX_OPTIONS_NONE);      // see http://docs.pixelmatix.com/SmartMatrix for options
const uint8_t kBackgroundLayerOptions = (SM_BACKGROUND_OPTIONS_NONE);
const uint8_t kIndexedLayerOptions = (SM_INDEXED_OPTIONS_NONE);
const uint8_t kScrollingLayerOptions = (SM_SCROLLING_OPTIONS_NONE);

//default functions
//SMARTMATRIX_ALLOCATE_INDEXED_LAYER(indexedLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kIndexedLayerOptions);
SMARTMATRIX_ALLOCATE_INDEXED_LAYER(indexedLayer2, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kIndexedLayerOptions);

SMARTMATRIX_ALLOCATE_SCROLLING_LAYER(scrollingLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kScrollingLayerOptions);

SMARTMATRIX_ALLOCATE_BUFFERS(matrix, kMatrixWidth, kMatrixHeight, kRefreshDepth, kDmaBufferRows, kPanelType, kMatrixOptions);
SMARTMATRIX_ALLOCATE_BACKGROUND_LAYER(backgroundLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kBackgroundLayerOptions);

GifDecoder<kMatrixWidth, kMatrixHeight, 12> decoder;

//Variables
char inChar; //this is the input character from the files that are read
const rgb24 defaultBackgroundColor = {0x0, 0, 0};   //sets default background char
//char seed[6]; //creates a char array of 6 chars

int mode=0; //sets default Mode which is random files
int totalDirs=62; //sets the number of directories on the SD card
int clockMode=1;
int randomFont=0;
int maxFonts=24;
int brightnessVal=100;
int menuChoice=0;
int startingDir=0;
int clockDisplayTime=5000;
int gifChoice=random(39);
int animationDir=random(62);
bool eggBool=false;


//menu buttons
Bounce blueButton = Bounce(11,500);
Bounce greenButton = Bounce(12,500);
Bounce redButton = Bounce(7,500);


//Gif functions

int num_files;
void screenClearCallback(void) {
  backgroundLayer.fillScreen({0,0,0});
}

void updateScreenCallback(void) {
  backgroundLayer.swapBuffers();
}

void drawPixelCallback(int16_t x, int16_t y, uint8_t red, uint8_t green, uint8_t blue) {

    backgroundLayer.drawPixel(x+80, y, {red, green, blue});
}


void setup() {
  

      
    //menu buttons
    pinMode(7,INPUT_PULLUP);
    pinMode(11, INPUT_PULLUP);
    pinMode(12,INPUT_PULLUP);

    

    //sets baud rate of 115200 for serial monitoring
    Serial.begin(115200);
    Serial.print("Game size");
    Serial.print(sizeof(gameNames));
    Serial.print("Initializing SD card...");

  if (!SD.begin(SD_CARD)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

//set the RTC
  setSyncProvider(getTeensy3Time);
   delay(200);
    if (timeStatus()!= timeSet) {
    Serial.println("Unable to sync with the RTC");
  } else {
    Serial.println("RTC has set the system time");
   setTime(hour(),minute(),second(),day(),month(),year());  
  
  }
  

  //gif stuff
    decoder.setScreenClearCallback(screenClearCallback);
    decoder.setUpdateScreenCallback(updateScreenCallback);
    decoder.setDrawPixelCallback(drawPixelCallback);

    decoder.setFileSeekCallback(fileSeekCallback);
    decoder.setFilePositionCallback(filePositionCallback);
    decoder.setFileReadCallback(fileReadCallback);
    decoder.setFileReadBlockCallback(fileReadBlockCallback);
    // Clear screen
    backgroundLayer.fillScreen({0,0,0});
    backgroundLayer.swapBuffers(false);
    //Debug to check number of files
    num_files = enumerateGIFFiles("/gif/", false);
    //Serial.println("num of gifs");
    //Serial.println(num_files);
    


  
    //sets the brightness of the matrix, add the display layers and start the matrix panel
    matrix.setBrightness(brightnessVal);
    matrix.addLayer(&backgroundLayer);
    matrix.addLayer(&indexedLayer2);
    matrix.addLayer(&scrollingLayer); 
    matrix.begin();
    //seeds the random number generator with a random read from an analog pin that has nothing in it
    randomSeed(analogRead(1));
    //just a quick delay before calling the menu
    delay(2000);
    menu();
    //spits out config settings to terminal (debug)
    //configSettings();

  //Gif Setup

   //backgroundLayer.enableColorCorrection(true);

}

void loop() {
  

  // This will run the menu.. this button does not debounce so holding it will constantly read low until it hits
  if(digitalRead(7) == LOW && digitalRead(11)==LOW){
      Serial.println("Break main loop with buttons");
      menu();
      configSettings();
  }
  
  //start each loop with a 1 second delay
  delay(1000);

  //This is how the clock works mode=0 is random (default), mode=1 is all files of the same game, mode=2, clock only
  if(mode==0){
    //check to see if the clock mode is turned on or not
    if(clockMode==1){
      //Serial.println("running clock");
      displayTime();
      //clear the screen after the time is up
      
    }
  //plays a random DMD animation
  playRandom();
  }else if(mode==1){
     
    playInOrder();
  
  }else if(mode ==2){
  displayTime();
  }
}


void playInOrder(){
    //This is the mode to play all images of one directory
    Serial.println("Images are in order");
    //a random animation directory is chosen during setup this adds one to it, if it is bigger than the total dirs it resets
    
    int animation = 0;  
  while(true){
    if(digitalRead(7) == LOW && digitalRead(11)==LOW){
      Serial.println("Called menu from playing in order");
      menu();
      break;
      }
    //waits a second before switching to the clock
    delay(1000);
    //runs the clock if the clock mode is on
    if(clockMode==1){
    displayTime();
    }
    
    //Serial.print("Current Directory:");
    //Serial.println(animationDir);
    //gets ready to load the file
    File directory;
    char dirString[4];
    //converts the random animationDir integer to a string
    sprintf(dirString,"%d",animationDir);
    directory=SD.open(dirString);
    int totalFiles = returnTotalFiles(directory);
    directory.close();
    //Serial.print("Total Animations in Dir: ");
    //Serial.println(totalFiles);
    //keeps track of the animation based on the total files in the directory
    char animationString[4];
    sprintf(animationString, "/%d/%d",animationDir,animation);
    animation+=1;
    //if animation is at its max then it resets
    if(animation==totalFiles){
      animation=0;
      animationDir+=1;
    }
    //if the total directories is over the max then it resets to 0
    if(animationDir==totalDirs){
      animationDir=0;
    }
    
    //Serial.print("the animation String is: ");
    //Serial.println(animationString);
    //loads the directoyy and animation as one string to load
    myFile = SD.open(animationString, FILE_READ);
    int delayVal;
    char fileDat[3];
    bool readDelay=true;
    //loops through the file getting ready to print
    while(myFile.available()){
      //will check to see if the second button is pressed and will replay the previous animation
      if(digitalRead(7)==LOW){
       
      if(animation>=2){
        animation-=2;
        myFile.close();
        break;
      }
      //has a small delay if the animation is not bigger than 2 for button debouncing
      delay(500);
      }
    //if third button is pressed it will jump to the next animation
    if(digitalRead(11)==LOW){
       delay(500);
       myFile.close();
       break;
    }
    //reads the delay between frames which is the first 3 chars of the animation file
    if(readDelay==true){
       myFile.read(fileDat,3);
       delayVal=atoi(fileDat);
       //Serial.println("reading the delay");
       //Serial.println(delayVal);
     
    }
   //sets readDelay to false so it only reads on the first time through 
   readDelay=false;
   delay(delayVal);
   //calls the draw function to draw the frame of the animation
   drawFrame(myFile);
   
  }myFile.close(); 
 
   
 }
}

void playRandom(){
  Serial.println("----------Start playRandom Function----------");
  //chooses random directory
  int randDir = random(totalDirs);
  Serial.print("Random Directory Selected: ");
  Serial.println(randDir);
  //the function to list directories requires a String so create this to put the random dir in
  char randDirString[4];
  //int into char array
  sprintf(randDirString,"%d",randDir);
  //specifies an object to read from the SD card
  Serial.print("Put the dir into a string: ");
  Serial.println(randDirString);
  File directory; 
  directory=SD.open(randDirString);
  int totalFiles = returnTotalFiles(directory);
  directory.close();
  Serial.print("Total Files in the Dir: ");
  Serial.println(totalFiles);
  //chooses random file based on the total files in the directory
  int randAnimation = random(totalFiles);
  Serial.print("Random file in dir is: ");
  Serial.println(randAnimation);
  char randAnimationString[4];
  //puts int into a string
  sprintf(randAnimationString, "/%d/%d",randDir,randAnimation);
  Serial.print("rand Animation String: ");
  Serial.println(randAnimationString);
  //reads the dir/animation string and opens the file
  myFile = SD.open(randAnimationString, FILE_READ);
  bool readDelay=true;
  int delayVal;
  char fileDat[3];
  Serial.println("------------------- just before draw -------------");
  while(myFile.available()){
  //first three chars are delay for the file to slow down or speed up if required
  if(readDelay==true){
       myFile.read(fileDat,3);
       delayVal=atoi(fileDat);
       //Serial.println("reading the delay");
       //Serial.println(delayVal);
     
    }
   //sets the delay to false so it won't read any more chars for delay 
   readDelay=false;
   delay(delayVal);
   drawFrame(myFile);
    if(digitalRead(11)==LOW){
       delay(500);
       myFile.close();
       break;
    }  

}myFile.close();
}


//a little gif function to play as an easter egg
void playGif(){
  scrollingLayer.setSpeed(70);
  
  scrollingLayer.setMode(wrapForward);
  scrollingLayer.start("SUPER SAIYAN EASTER EGG",5);
  eggBool=true;
  openGifFilenameByIndex("/egg/", 0);
  decoder.startDecoding();
  int startTime=millis();
  int endTime=0;
  int nowTime=0;
  while(endTime<15000){
    nowTime=millis();
    endTime=nowTime-startTime;
    decoder.decodeFrame();
  } 
  backgroundLayer.fillScreen({0,0,0});
  backgroundLayer.swapBuffers();
  scrollingLayer.stop();
  eggBool=false;
}

//function to play the entire directory
void playDir(int startingDir){
    
      
    int animation=0;
    File directory;
    char dirString[4];
    char animationString[4];
    sprintf(dirString,"%d",startingDir);
    directory=SD.open(dirString);
    int totalFiles = returnTotalFiles(directory);
    directory.close();
    Serial.println("total files");
    Serial.println(totalFiles);
    
   
   while(true){
    //one second between animations
    delay(1000);
    //will check to see if all animations in the dir have played then it will break to menu
    if(animation>totalFiles){
      Serial.println("animation: ");
      Serial.println(animation);
      break;
    }    
    //creates the string to play
    sprintf(animationString, "/%d/%d",startingDir,animation);
    //preps animation for the next iteration
    animation+=1;
    //will break from playing if both these buttons are pressed
    if(digitalRead(7)==LOW && digitalRead(11)==LOW){
      break;
    }
    
    Serial.print("the animation String is: ");
    Serial.println(animationString);
    myFile = SD.open(animationString, FILE_READ);
    int delayVal;
    char fileDat[3];
    bool readDelay=true;
    //iterates through the frames in the file
    while(myFile.available()){
      //plays the previous animation
      if(digitalRead(7)==LOW){
        
        if(animation>=2){
          animation-=2;
          myFile.close();
          break;
         }delay(500);
    }
    //plays the next animation
    if(digitalRead(11)==LOW){
       delay(500);
       myFile.close();
       break;
    }
    if(readDelay==true){
      //reads the first 3 chars in the file and uses it to determine the delay has a bool so it only reads once since the delay
      //is on at the beginning of the scenes
       myFile.read(fileDat,3);
       delayVal=atoi(fileDat); 
        
    }
    readDelay=false;
    delay(delayVal);
    
    //each DMD image will be 4160 dots so this will run 4160 times
    //calls the function that will draw one frame
    drawFrame(myFile);
        
    }myFile.close();
  
};
   
  
  
  
}

//function that draws all the frames of the animation
void drawFrame(File myFile){
  //if a new line is found then it will skip the char and increase the height of the matrix and restart the x value
      int x=0;
      int xwidth=0;
      int yheight=0;
      //Serial.println("draw");
      //each animation is 4160 values
      while(x<4160){
      //reads a characted in the file
      inChar = char(myFile.read());
      //if it reads a newline char then it goes to the next line 
      if(inChar=='\n'){
        x++;
        yheight++;
      }else{
      //This will see what the character is and put a certain shade of color depending on what value was found in the image file
        if((char)inChar=='1'){
          backgroundLayer.drawPixel(xwidth,yheight,{0x10,0,0});  
          // Serial.println("red");
            xwidth++;
        }else if((char)inChar=='2'){
          backgroundLayer.drawPixel(xwidth,yheight,{0x10,0x0,0x00});
          xwidth++;
        }else if((char)inChar=='3'){
          backgroundLayer.drawPixel(xwidth,yheight,{0x40,0x00,0x00});
          xwidth++;
        }else if((char)inChar=='4'){
          backgroundLayer.drawPixel(xwidth,yheight,{0x40,0x00,0x00});
          xwidth++;
        }else if((char)inChar=='5'){
          backgroundLayer.drawPixel(xwidth,yheight,{0x80,0x00,0x00});
          xwidth++;
        }else if((char)inChar=='6'){
          backgroundLayer.drawPixel(xwidth,yheight,{0x80,0x00,0x00});
          xwidth++;
        }else if((char)inChar=='7'){
          backgroundLayer.drawPixel(xwidth,yheight,{0xA0,0x00,0x00});
          xwidth++;
      
        }else if((char)inChar=='8'){
          backgroundLayer.drawPixel(xwidth,yheight,{0xff,0x00,0x00});
          xwidth++;
      
        } else{
          backgroundLayer.drawPixel(xwidth,yheight,{0,0,0});
          xwidth++;
        }
        //if the width ends up being 129 then it resets it to 0
        //if the width of the dmd is greater than 128 then it resets to 0
        if(xwidth ==129){
          xwidth=0;
        }
        //if the height is bigger than 33 it resets to zero
        if(yheight >=33){
          yheight=0;
        }
        x++;
   }
       
}
//swapBuffers draws the frame after all of the dots are drawn which makes it fast
backgroundLayer.swapBuffers();
}

//function that draws the time
void displayTime(){
    //random numbers are chose so colors are random
    int r = (int) random(20,255); //sets random Red value
    int g = (int) random(20,255); //sets random Green Value
    int b = (int) random(20,255); //sets random Blue value

    //adds 1 to the gif that will play
    gifChoice+=1;
    //resets the gifs if == to 39
    if(gifChoice==39){
      gifChoice=0;
    }
    //chooses the gif by index, this gif will play next to the clock
    Serial.println("Choosing GIF");
    openGifFilenameByIndex("/gif/",gifChoice);
    Serial.println("Gif is: ");
    Serial.println(gifChoice); 
    //starts decoding the gif
    decoder.startDecoding();
    //defaults true if the colon appears in the time
    int xOffset=1;             
    int xwidth=0;
    int yheight=0;
    //clears the layers after the last animation before drawing the time
    backgroundLayer.fillScreen(defaultBackgroundColor);
    backgroundLayer.swapBuffers();
    
   // sets the color for the colon
    indexedLayer2.setIndexedColor(2,{r*0.8,g*0.8,b*0.8});
   // need a string pointer for the fonts that will be used
   String* usedFont;
   
   //if this value is set to zero in the settings then random fonts for the time will be used
   if(randomFont==0){ 
   //Chooses a random font if randFont is initially set to zero from the settings 
   int randFont=random(maxFonts);
   //0 isn't a font so if that is the number chosen then it just goes to the next font
   if(randFont==0){
      randFont+=1;
   }
   usedFont=fonts[randFont]; 
   Serial.print("Used font[] is: ");
   Serial.println(fontNames[randFont]);
   }else{
      //if randFont is set to something then it just uses that font forever   
      usedFont=fonts[randomFont]; 
   }
   //This code parses out the time into a value that is 4 characters long
   String displayHour;
   int numHour=hour();
   Serial.print("Hour is:");
   Serial.println(numHour);
   int numMinutes = minute();
   //Serial.println("Minute is:");
   //Serial.println(numMinutes);
   String displayMinutes;
   if(numHour<10){
    displayHour=" "+String(numHour);
   }else{
    displayHour=String(numHour);
   }
   if(numMinutes<10){
    displayMinutes="0"+String(numMinutes);
   }else{
    displayMinutes=String(numMinutes);
   }
   String displayTime = displayHour+displayMinutes;
   Serial.print("displayTime: ");
   Serial.println(displayTime);
   
   //This loop goes through the four values of Time
   for(int i=0; i<4; i++){
    int letterToDraw=0;
    int yOffset=8;
    int x=0;
    //Adds extra space between the 2nd and third number for the colon
    if(i==2){
      xOffset+=7;
    }
    //each font number is always 252 pixels
    int sizeOfImage=252;
    //determines what value is in which spot 
    if(displayTime[i]==':'){
      letterToDraw=10;    
    }else if(displayTime[i]=='1'){
      letterToDraw=1;
    }else if(displayTime[i]=='2'){
      letterToDraw=2;
    }else if(displayTime[i]=='3'){
      letterToDraw=3;
    }else if(displayTime[i]=='4'){
      letterToDraw=4;
    }else if(displayTime[i]=='5'){
      letterToDraw=5;
    }else if(displayTime[i]=='6'){
      letterToDraw=6;
    }else if(displayTime[i]=='7'){
      letterToDraw=7;
    }else if(displayTime[i]=='8'){
      letterToDraw=8;
    }else if(displayTime[i]=='9'){
      letterToDraw=9;
    }else if(displayTime[i]=='0'){
      letterToDraw=0;
    }
   
    
    //draw loop to print the dots of each letter
    while(x<sizeOfImage){
        //checks to see which number to draw and the color    
        if(usedFont[letterToDraw][x]=='1'){
          backgroundLayer.drawPixel(xwidth+xOffset,yheight+yOffset,{r*.40,g*0.40,b*0.40});  
          // Serial.println("red");
          xwidth++;
        }else if(usedFont[letterToDraw][x]=='2'){
          backgroundLayer.drawPixel(xwidth+xOffset,yheight+yOffset,{r*0.55,g*0.55,b*0.55});
          xwidth++;
        }else if(usedFont[letterToDraw][x]=='3'){
          backgroundLayer.drawPixel(xwidth+xOffset,yheight+yOffset,{r*0.70,g*0.7,b*0.7});
          xwidth++;
        }else if(usedFont[letterToDraw][x]=='4'){
          backgroundLayer.drawPixel(xwidth+xOffset,yheight+yOffset,{r*0.9,g*0.9,b*0.9});
          xwidth++;
        }else if(usedFont[letterToDraw][x]=='5'){
          backgroundLayer.drawPixel(xwidth+xOffset,yheight+yOffset,{r,g,b});
          xwidth++;
        }else if(usedFont[letterToDraw][x]=='6'){
          backgroundLayer.drawPixel(xwidth+xOffset,yheight+yOffset,{255*0.7,255*0.7,255*0.7});
          xwidth++;
        }else{ 
          xwidth++;      
        }
      //each font is 14 dots wide so if it is bigger than 14 it resets the x and adds starts a new line
      if(xwidth >=14){
        xwidth=0;
        yheight++;
    
      }
      //each font is 18 chars hight so if the font is bigger than 18 then it resets and adds 15 dots space to the next char
      if(yheight >=18){
        yheight=0;
        xOffset+=15;
        //  Serial.println("?");
      }x++;
      
      
     
  }
  //swaps buffers and draws after dots are set so it draws fast
  backgroundLayer.swapBuffers();
} 
    //Used to draw the colon and ensure that the clock displays for the time chosen in the menu settings (5 secs is default).
    int currentTime;
    int initialTime=millis();
    int futureTime = millis()+clockDisplayTime; //clock display time is the length of time in the settings the clock will show for (5 sec default).

    //looop to play for display time
    while(currentTime<futureTime){
    currentTime=millis();
    int colonTime;
    int x=0;
    int y=0;
    //Serial.println(usedFont[11]);
    for(int i=0; i<252;i++){
        //iteration code for the colon
        if(usedFont[10][i]!='0'){
          indexedLayer2.drawPixel(x+26,y+8,2);   
          xwidth++;
        }else{}
          if (x==14){
            x=0;
            y++;
          }
    if(y==18){
      y=0;
    }x++;
    
    
    }
    //iterates through the gif
    decoder.decodeFrame();
     
    //draws the colon
    indexedLayer2.swapBuffers();
    }
    //clear the screen after the time is done showing
    indexedLayer2.fillScreen(0);
    backgroundLayer.fillScreen({0,0,0});   
    indexedLayer2.swapBuffers();
    backgroundLayer.swapBuffers();
}

//stock code to set the teensy RTC 
time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}

//integer returning function to determine which menu choice
int menuOption(){
    int localSelection;
    indexedLayer2.fillScreen(0);
    indexedLayer2.swapBuffers();
    //draws menu options based on the value of the menuChoice
    if(menuChoice==1){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Exit");
      backgroundLayer.swapBuffers();
      localSelection = 0;
      
    }else if(menuChoice==5){
   
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Set Time");
      backgroundLayer.swapBuffers();
      localSelection = 1;
      
    }else if(menuChoice==2){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Set Mode");
      backgroundLayer.swapBuffers();
      localSelection = 3;
      
    }else if(menuChoice==3){
      if(clockMode==1){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock On");
      backgroundLayer.swapBuffers();
      }else{
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock off");
        backgroundLayer.swapBuffers();
        clockMode=1;
      }
      localSelection = 4;
    }else if(menuChoice==4){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      char tempName[20];
      int tempVal=0;
      if(randomFont==0){
        tempVal=1;
      }else{
        tempVal=randomFont;
      }
      fontNames[tempVal].toCharArray(tempName,20);
        
      //Serial.println(fontNames[tempVal]);
      backgroundLayer.drawString(50, matrix.getScreenHeight() / 2, {0xff,0,0}, tempName);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Font:");
      backgroundLayer.swapBuffers();
        
      localSelection = 5;
      
    }else if(menuChoice==0){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Panel Reset");
      backgroundLayer.swapBuffers();
      localSelection = 2;         
    }else if(menuChoice==6){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Play Directory");
      backgroundLayer.swapBuffers();
      localSelection = 6;        
    }else if(menuChoice==7){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, " Set Brightness");
      backgroundLayer.swapBuffers();
      localSelection = 7;
      
    }else if(menuChoice==8){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, " Set Clock Delay Time");
      backgroundLayer.swapBuffers();
      localSelection = 8;
    }else {
      //catch in case somehow a wrong menu choice is made
      Serial.print("Selection choice Error, MenuChoice is: ");
      Serial.println(menuChoice);
    }
    return localSelection;
    }

void menu() {
  //menu variable
  //easter egg hunting?  But it isn't Easter is it?!
  int egg=0;
  int selection=2;
  int maxMenuItem=9; //this is the max number of menu items so it can be changed in one spot during dev
  bool menuExit=false;
  String setHour;

  //Set up menu display
  Serial.println("Menu Started");
  Serial.println(menuChoice);
  scrollingLayer.setColor({0xff, 0xff, 0xff});
  scrollingLayer.setSpeed(40);
  backgroundLayer.fillScreen(defaultBackgroundColor);
  scrollingLayer.setMode(wrapForward);
  
  //Default Option so its easy to reset due to the shitty chinese LED controller chips  
  backgroundLayer.setFont(font3x5);
  backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Reset Display");
  while(scrollingLayer.getStatus());
  backgroundLayer.swapBuffers();

  //Menu Loop
  while(menuExit==false){
  if(!scrollingLayer.getStatus()){
    scrollingLayer.start("Menu", 2);
  }
    blueButton.update();
    greenButton.update();
    redButton.update();
    int value = blueButton.read();
    int value1= greenButton.read();
    int value2= redButton.read();
   if(egg==20){
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.swapBuffers();
      playGif();
      Serial.println("no delay");
      egg=0;
      scrollingLayer.setSpeed(40);
      
    }
   
   //button 1 push will move forward through menu;  It sets call the menuOption function which will 
   //set the selection variable and change the display to show the option that can be chosen
   if(value==LOW){
  //  Serial.println("Button1 pushed");
      egg++;

    //'if' to loop back to the start of the menu options
    if(menuChoice==maxMenuItem-1){
      menuChoice=0;
    }else{
      menuChoice+=1;
    }
    selection = menuOption();
    delay(500);
    }
   //Button 3 push
    if(value2==LOW){
      egg++;
    
    //quick 'if' to loop back to the last menu options
    if(menuChoice==0){
      menuChoice=maxMenuItem-1;
    }else{
    menuChoice-=1;
    }
    selection=menuOption();
    Serial.println("selection: " );
    Serial.println(selection);
    delay(500);
   }
   
   //now that the selection is set,  hitting button 2 will activate the following choice 
   if(value1==LOW){
      if(selection ==1){
        Serial.println("Set Time");
        delay(500); //delays before to prevent button bounce
        setRTC();
      }else if(selection==2){
        Serial.println("Reset the display");
        delay(500);      
        setupReset();
        delay(1000);
        displayReset();
      }else if(selection==3){
        delay(500);
        modeSet(); 
        //puts the last mode option back on the display
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Set Mode");
        backgroundLayer.swapBuffers();
      }else if(selection==4){
        //this sets turns the clock display off so only animations will play
        delay(500);
        if(clockMode==0){
          backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock On");
        backgroundLayer.swapBuffers();
        clockMode=1;
        }else{
            backgroundLayer.fillScreen(defaultBackgroundColor);
            backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock off");
            backgroundLayer.swapBuffers();
            clockMode=0;
        }
        
      }else if(selection ==5){
        //font options
        delay(500);
        indexedLayer2.fillScreen(0);
        indexedLayer2.swapBuffers();
        chooseFont();
        //resets the display back to the last option
        indexedLayer2.fillScreen(0);
        indexedLayer2.swapBuffers();
        backgroundLayer.fillScreen(defaultBackgroundColor);
        char tempName[50];
        int tempVal=randomFont;
        if(tempVal==maxFonts){
          tempVal=0;
        }
        fontNames[tempVal].toCharArray(tempName,50);
        Serial.println(tempName);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Font:");
        backgroundLayer.drawString(30, matrix.getScreenHeight() / 2, {0xff,0,0}, tempName);
        backgroundLayer.swapBuffers();
     
      }else if (selection == 7){
        Serial.println("Set brightness");
        delay(500);
        changeBrightness();
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Set Brightness");
        backgroundLayer.swapBuffers();
      }else if(selection ==0){
        Serial.println("exit menu");
        menuExit=true;
        
      }else if(selection==6){
        //play specific dirs for fun
        delay(500);
        int dirToPlay = chooseFolder();
        Serial.println("The directory is:");
        Serial.println(dirToPlay);
        scrollingLayer.stop();
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.swapBuffers();
        playDir(dirToPlay);
        indexedLayer2.fillScreen(0);
        indexedLayer2.swapBuffers();
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Play Directory");
        backgroundLayer.swapBuffers();
      } else if(selection ==8){
        Serial.println("Clock Display Time");
        delay(500);
        changeClockDelay();
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock Display time");
        backgroundLayer.swapBuffers();
      } else{
        Serial.print("invalid options something went wrong. Selection value is: ");
        Serial.println(selection);
      }
  }
  
}       //stops the menu graphics before exit
        scrollingLayer.stop();
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.swapBuffers();
}

//function to change the brightness value
void changeBrightness(){
        char brightString[3];
        Serial.println("Brightness");
        Serial.println(brightnessVal);
        sprintf(brightString,"%d",brightnessVal);
        //sets up display for brightness menu
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Brightness:");
        backgroundLayer.drawString(70, matrix.getScreenHeight() / 2, {0xff,0,0}, brightString);
        backgroundLayer.swapBuffers();
        scrollingLayer.stop();
        scrollingLayer.start("Set Brightness", 2);
        //loop while setting the value
        while(true){
           if(!scrollingLayer.getStatus()){
            scrollingLayer.start("Set Brightness", 2);
            }
          //subtracts the value on this setting  
          if(digitalRead(7)==LOW){
            delay(500);
            //lowest val is 0 then it resets to 100
            brightnessVal-=10;
            if(brightnessVal<0){
              brightnessVal=100;
            }
            //updates the setting
            matrix.setBrightness(brightnessVal);
            sprintf(brightString,"%d",brightnessVal);
            //shows the current setting
            backgroundLayer.fillScreen(defaultBackgroundColor);
            backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Brightness:");
            backgroundLayer.drawString(70, matrix.getScreenHeight() / 2, {0xff,0,0}, brightString);
            backgroundLayer.swapBuffers();
          }
          //increases the brightness
          if(digitalRead(11)==LOW){
            delay(500);
            brightnessVal+=10;
            //max value is 100 then it resets to 0
            if(brightnessVal>100){
              brightnessVal=0;
            }
            matrix.setBrightness(brightnessVal);
            sprintf(brightString,"%d",brightnessVal);
            backgroundLayer.fillScreen(defaultBackgroundColor);
            backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Brightness:");
            backgroundLayer.drawString(70, matrix.getScreenHeight() / 2, {0xff,0,0}, brightString);
            backgroundLayer.swapBuffers();
          }
          //this button will lock in the setting
          if(digitalRead(12)==LOW){
            delay(500);
            scrollingLayer.stop();
            break;
          }
        }
  
}

//function to change how long the clock displays for
void changeClockDelay(){
        char clockTime[3];
        Serial.print("Clock display time: (in millis) ");
        Serial.println(clockDisplayTime);
        //setting is in millis so you divide by 1000 to get the seconds
        sprintf(clockTime,"%d",clockDisplayTime/1000);
        //sets up the menu
        backgroundLayer.fillScreen(defaultBackgroundColor);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock Display (secs):");
        backgroundLayer.drawString(100, matrix.getScreenHeight() / 2, {0xff,0,0}, clockTime);
        backgroundLayer.swapBuffers();
        scrollingLayer.stop();
        scrollingLayer.start("Clock Display Time", 2);
        //loop while setting the value
        while(true){
           if(!scrollingLayer.getStatus()){
            scrollingLayer.start("Clock Display Time", 2);
            }
          //subtract the value by 1000 min is 5000 (5 secs)
          if(digitalRead(7)==LOW){
            delay(500);
            clockDisplayTime-=1000;
            if(clockDisplayTime<5000){
              clockDisplayTime=15000;
            }
            //update the display
            sprintf(clockTime,"%d",clockDisplayTime/1000);
            backgroundLayer.fillScreen(defaultBackgroundColor);
            backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock Display (secs):");
            backgroundLayer.drawString(100, matrix.getScreenHeight() / 2, {0xff,0,0}, clockTime);
            backgroundLayer.swapBuffers();
          }
          //increase the value max is 15000 (15 secs);
          if(digitalRead(11)==LOW){
            delay(500);
            clockDisplayTime+=1000;
            if(clockDisplayTime>15000){
              clockDisplayTime=5000;
            }
            //update the display
            sprintf(clockTime,"%d",clockDisplayTime/1000);
            backgroundLayer.fillScreen(defaultBackgroundColor);
            backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock Display (secs):");
            backgroundLayer.drawString(100, matrix.getScreenHeight() / 2, {0xff,0,0},clockTime);
            backgroundLayer.swapBuffers();
          }
          //lock in the setting and back to main menu
          if(digitalRead(12)==LOW){
            delay(500);
            scrollingLayer.stop();
            break;
          }
        }
  
}

//int function to return value of the folder that is chosen
int chooseFolder(){
     int selectedNumber=0;
     //update menu display
     backgroundLayer.fillScreen(defaultBackgroundColor);
     backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Choose Game");
     backgroundLayer.swapBuffers();
     scrollingLayer.stop();
     scrollingLayer.start("Choose Game", 2);
     //loop while choosing
     while(true){
        if(!scrollingLayer.getStatus()){
        scrollingLayer.start("Choose Game", 2);
    }
    //old way to debounce buttons.. too lazy to change, works as per bounce library
    blueButton.update();
    greenButton.update();
    redButton.update();
    int value = blueButton.read();
    int value1= greenButton.read();
    int value2= redButton.read();
    
    if(value==LOW){
      delay(500);
      //Serial.println("button 1");
      selectedNumber+=1;
      
      if(selectedNumber>totalDirs-1){
        selectedNumber=0;
      }
      //Serial.print("Selected Number is: ");
      //Serial.println(selectedNumber);
        backgroundLayer.fillScreen(defaultBackgroundColor);
        char tempName[20];
        gameNames[selectedNumber].toCharArray(tempName,20);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, tempName);
        backgroundLayer.swapBuffers();
    }
    if(value2==LOW){
      //Serial.println("Button 2");
      delay(500);
      //Serial.println(selectedNumber);
      selectedNumber-=1;
      if(selectedNumber<0){
        selectedNumber=totalDirs-1;
      }
        //Serial.print("Selected Number is: ");
        //Serial.println(selectedNumber);
        backgroundLayer.fillScreen(defaultBackgroundColor);
        char tempName[20];
        gameNames[selectedNumber].toCharArray(tempName,20);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, tempName);
        backgroundLayer.swapBuffers();
    }
    if(value1==LOW){
      delay(500);
      break;
    }
  }
  //Serial.println("Selected Number is:");
  //Serial.println(selectedNumber);
  return selectedNumber;
}

//function draw fonts during the selection of the menu
void drawFonts(int fontChoice){
  
  //if random then do nothing
  if(fontChoice==0){
    
  }else{
  //create a string of the current font choice to draw
  String* currentFont = fonts[fontChoice];
  int number=0;  
  delay(500);
  for(int number=0;number<10;number++){
        //draw the font displayed   
        indexedLayer2.fillScreen(0);
        indexedLayer2.swapBuffers();
        int x=0; // x iterator
        int y=0;//y iterator
        for(int i=0; i<252;i++){ //252 is the size of each font image
          if (x==14){
            x=0;
            y++;
          }
          if(y==18){
            y=0;
          }x++;
          if(currentFont[number][i]!='0'){
            indexedLayer2.drawPixel(x+90,y+10,2);
         }  
    }
    indexedLayer2.swapBuffers();
    //delay 100 secs between numbers being drawn
    delay(100);
   
    }
  }
}

//funtion that displays the choices of fonts
void chooseFont(){
    int value,value1,value2;
    bool menuExit= false;
    int fontChoice=0;
    //prep the drawing of the menu option
    scrollingLayer.setColor({0xff, 0xff, 0xff});
    scrollingLayer.setSpeed(40);
    scrollingLayer.setMode(wrapForward);
    backgroundLayer.fillScreen(defaultBackgroundColor);
    backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Press button to choose font");
    backgroundLayer.swapBuffers();
    indexedLayer2.setIndexedColor(3,{255,0,0});
    //loop while choosing
    while(menuExit==false){
       if(!scrollingLayer.getStatus()){
         scrollingLayer.start("Font Choice", 2);
       }
      //button bounce updates 
      blueButton.update();
      greenButton.update();
      redButton.update();
      value = blueButton.read();
      value1= greenButton.read();
      value2= redButton.read();
      //if button pressed choose font
      if(value1==LOW){
        randomFont=fontChoice;
        
        Serial.print("randomFont: ");
        Serial.println(randomFont);
        delay(500);
        break;
      }
      //if pressed then go back to the prev. font
      if(value2==LOW){
         delay(500);
        fontChoice-=1;
        //go to the last font if below zero
        if(fontChoice<0){
          fontChoice=maxFonts-1;
        } 
        //clear the display of the last font
        indexedLayer2.fillScreen(0);
        indexedLayer2.swapBuffers(); 
        //draw the name of the font 
        backgroundLayer.fillScreen(defaultBackgroundColor);
        char tempName[20];
        fontNames[fontChoice].toCharArray(tempName,20);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, tempName);
        backgroundLayer.swapBuffers();
        drawFonts(fontChoice); 
     
      }
      //increase to the next font choice... if past the max then go to the first choice
      if(value==LOW){
        delay(500);
        Serial.println("Green button");
        fontChoice+=1; 
        if(fontChoice>=maxFonts){
          fontChoice=0;
        }
        //clear the screen of the last font choice
        indexedLayer2.fillScreen(0);
        indexedLayer2.swapBuffers(); 
        backgroundLayer.fillScreen(defaultBackgroundColor);
        //draw the font name
        char tempName[20];
        fontNames[fontChoice].toCharArray(tempName,20);
        backgroundLayer.drawString(10, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, tempName);
        backgroundLayer.swapBuffers();
        drawFonts(fontChoice); 
 
        }
    }

}

//function to set the mode of operation
void modeSet(){
    int localMode=0;
    int buttonPush;
    int exitButton;
    int button3Push;
    bool menuExit= false;
    //draw the menu options
    scrollingLayer.setColor({0xff, 0xff, 0xff});
    scrollingLayer.setSpeed(40);
    backgroundLayer.fillScreen(defaultBackgroundColor);
    scrollingLayer.setMode(wrapForward);
    backgroundLayer.fillScreen(defaultBackgroundColor);
    
    //loop while choosing  
    while(menuExit==false){
       if(!scrollingLayer.getStatus()){
        scrollingLayer.start("Set mode", 2);
        }
      //button updates for debounce  
      blueButton.update();
      greenButton.update();
      redButton.update();
      buttonPush=blueButton.read();
      exitButton=greenButton.read();
      button3Push=redButton.read();
      //change option no matter what button is pressed
      if(buttonPush==LOW || button3Push==LOW){
      //  Serial.println("button push");
        delay(500);
        localMode+=1;
        if(localMode>2){
          localMode=0;
        }
      }
      //exit when this button is pressed and select the option
      if(exitButton==LOW){
        mode=localMode;
        Serial.print("mode=");
        Serial.print(mode);
        delay(500);
        break;
        
      }
      //change the display as the buttons are pressed     
      if(localMode==0){
       
          backgroundLayer.fillScreen(defaultBackgroundColor);
          backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Random Images");
          backgroundLayer.swapBuffers();
      }else if(localMode==1){
          backgroundLayer.fillScreen(defaultBackgroundColor);
          backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Images will appear by game");
          backgroundLayer.swapBuffers();
      }else if(localMode==2){
          backgroundLayer.fillScreen(defaultBackgroundColor);
          backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Clock Only");
          backgroundLayer.swapBuffers();
      }
      
   
   
    }
}

//function to manually set the clock time in the menu
void setRTC(){
  
  
  int setHour;
  int setMinute;
  int setSecond;
  int setDay;
  int setMonth;
  int setYear;
  
  
  char charHours[]="Set Hour";
  char charMinute[]="Set Minutes";
  char charSecond[]="Set Seconds";
  char charDay[]="Set Day";
  char charMonth[]="Set Month";
  char charYear[]="Set Year";
  //draw the current option being set
  backgroundLayer.fillScreen(defaultBackgroundColor);
  backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Set Hour");
  backgroundLayer.drawString(80, matrix.getScreenHeight() / 2, {0xff, 0x0, 0x0}, "00");
  backgroundLayer.swapBuffers();
  //set the time based on the option
  setHour= setTimeValue(charHours,9,22); 
  setMinute = setTimeValue(charMinute,9,58);
  setSecond = setTimeValue(charSecond,9,58);
  setDay = setTimeValue(charDay,7,7);
  setMonth = setTimeValue(charMonth,9,11);
  setYear  = setTimeValue(charYear,9,50)+2000;
  setTime(setHour,setMinute,setSecond,setDay,setMonth,setYear);
  Serial.print("hour: ");
  Serial.println(setHour);
  Serial.print("Minutes: ");
  Serial.println(setMinute);
  Serial.print("seconds: ");
  Serial.println(setSecond);
  Serial.print("Day: ");
  Serial.println(setDay);
  Serial.print("Month: ");
  Serial.println(setMonth);
  Serial.print("Year: ");
  Serial.println(setYear);
  backgroundLayer.fillScreen(defaultBackgroundColor);
  backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, "Time Set!");
  backgroundLayer.swapBuffers();
}

//int function that returns the time being set
int setTimeValue(char timeBeingSet[],int time1Max, int time2Max){
  int timeToSet;
  bool timeset=false;
  int time1;
  int time2;
  char timeChar[]="01";
 
  String test1=timeBeingSet;  
  //pics the option to set based on what the string is
  if(test1.equalsIgnoreCase("Set Day") || test1.equalsIgnoreCase("Set Month")){
   // Serial.println("values Set on day/month");
   
   time1=1;
   time2=0;   
  }else{
    time1=0;
    time2=0;
  }
  //loop while selecting the value
  while(timeset==false){
      blueButton.update();
      greenButton.update();

      //increase values with this button
      if(blueButton.read()==LOW){
        delay(500);
         time1+=1;
      if(time1>time1Max){
         time1=0;
         String test1=timeBeingSet;
         if(test1.equalsIgnoreCase("Set Day")){
            time2=0;
            time1=1;
         }else{
            time2+=1;
          }
      }
      String test = timeChar;
    
      if(test.toInt()>time2Max){
        time2=0;
        time1=0;
      }
      timeChar[1]='0'+(int)time1;
      timeChar[0]='0'+(int)time2;
          
     }
      //set the value with this button     
      if(greenButton.read()==LOW){
        delay(500);
        Serial.println("Timechar is: ");
        Serial.println(timeChar);
        String test2=timeChar; 
        timeToSet= test2.toInt();
        timeset=true;
      }
      //draw value of time being set
      backgroundLayer.fillScreen(defaultBackgroundColor);
      backgroundLayer.drawString(20, matrix.getScreenHeight() / 2, {0xff, 0xff, 0xff}, timeBeingSet);
      backgroundLayer.drawString(80, matrix.getScreenHeight() / 2, {0xff, 0x0, 0x0}, timeChar);
      backgroundLayer.swapBuffers();
   }
   //returns the choice
   return timeToSet;
}

//function to display the current settings.  All settings are in ram and not on disk.  They reset to default upon power loss.
void configSettings(){
    Serial.println("");
    Serial.println("Settings");
    Serial.println("----------");
    Serial.println("");
    Serial.print("Brightness: ");
    Serial.println(brightnessVal);
    if(clockMode==0){
      Serial.println("Clock is Turned: Off");
    }else{
      Serial.println("Clock is Turned: On");
    }
    if(randomFont==0){
      Serial.println("Font is set to: random");
    }else{
      Serial.print("Font is set to: ");
      Serial.println(fontNames[randomFont-1]);
    }
    if(mode==1){
    Serial.print("Animations playing: randomly");
    }else{
    Serial.print("Animations playing: in order by dir");
    }
    //time check settings: 
    String displayHour;
    int numHour=hour();
    int numMinutes = minute();
    String displayMinutes;
   if(numHour<10){
    displayHour=" "+String(numHour);
   }else{
    displayHour=String(numHour);
   }
   if(numMinutes<10){
    displayMinutes="0"+String(numMinutes);
   }else{
    displayMinutes=String(numMinutes);
   }
   String displayTime = displayHour+displayMinutes;
    
    Serial.println("");
    Serial.println("TIME SETTINGS");
    Serial.println("--------------");
    Serial.println("");
    Serial.print("Hour: ");
    Serial.println(displayHour);
    Serial.print("Minute: ");
    Serial.println(displayMinutes);
    Serial.print("Time: ");
    Serial.println(displayTime);

    Serial.println("");
    Serial.println("SD CARD");
    Serial.println("-----------");
    Serial.println("");
    
    Serial.print("Total Directories (manually set): "); 
    Serial.println(totalDirs);

}

//this function returns the number of files in a directory on the SD card
int returnTotalFiles(File dir) {
   int fileCount=0;
   while(true) {
     
     File entry =  dir.openNextFile();
     if (! entry) {
       // no more files
       //Serial.println(fileCount);
       return fileCount;
     }
     fileCount+=1;
     entry.close();
   }
}
